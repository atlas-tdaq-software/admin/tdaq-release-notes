
# Introduction 

[![Doxygen](https://img.shields.io/badge/Doxygen-9.4.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/doxygen/tdaq-09-04-00/html/index.html)
[![Javadoc](https://img.shields.io/badge/Javadoc-9.4.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-09-04-00/index.html)


The ATLAS TDAQ software version **`tdaq-09-04-00`** has been released 
on 16th September 2021.

## Availability and Installation

Outside of Point 1 the software should be used via CVMFS. It's official
location is 

    /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-04-00/

At Point 1 the software is as usual available at

    /sw/atlas/tdaq/tdaq-09-04-00/

The software
can also be installed locally via [ayum](https://gitlab.cern.ch/atlas-sit/ayum).

    git clone https://gitlab.cern.ch/atlas-sit/ayum.git
    source ayum/setup.sh

Modify the `prefix` entries in the yum repository files in `ayum/etc/yum.repos.d/*.repo`
to point to the desired destination.

    ayum install tdaq-09-04-00_x86_64-centos7-gcc11-opt

In case the LCG RPMs are not found, add this to etc/yum.repos.d/lcg.repo:

```
[lcg-repo-101]
name=LCG 101 Repository
baseurl=http://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/LCG_101/
enabled=1
prefix=[...your prefix...]
```

## Configurations

The release is available for the following configurations:

* x86_64-centos7-gcc8-opt
* x86_64-centos7-gcc8-dbg
* x86_64-centos7-gcc11-opt (default at Point 1)
* x86_64-centos7-gcc11-dbg
* x86_64-centos8-gcc11-opt (experimental)
* x86_64-centos8-gcc11-dbg (experimental)

## External Software

### LCG_101

The version of the external LCG software is [LCG_101](http://lcginfo.cern.ch/release/101/).


### CORAL and COOL - 3.3.9

These two packages are no longer part of LCG. They are included in tdaq-common
and their use should be transparent for users.

There are several CORAL libraries which are really loadable plugins, and
user should not link against them. In the past this was possible, but
in this release only the offical targets are available. 

If you get a link error, remove the CORAL library if is not in this list
and see if things work.

The official libraries are:

* CORAL::CoralBase          
* CORAL::CoralServerBase   
* CORAL::CoralMonitor 
* CORAL::CoralStubs
* CORAL::CoralCommon        
* CORAL::CoralServerProxy
* CORAL::CoralKernel        
* CORAL::CoralSockets 

### TDAQ Specific External Software

 Package         | Version |  Requested by
 ----------------|---------|----------------
 beautifulsoup4  | 4.10.0  |  L1Calo
 cmzq            | 3.0.2   |  FELIX
 zyre            | 1.1.0   |  FELIX
 libfabric       | 1.11.0  |  FELIX
 colorama        | 0.4.4   |  DCS
 opcua           | 0.98.13 |  DCS
 parallel-ssh    | 2.6.0.post1 | TDAQ/PartitionMaker
 pugixml         | 1.9     |  L1Calo, L1CTP
 ipbus-software  | 2.8.1   |  L1Calo, L1CTP
 microhttpd      | 0.9.73  |  TDAQ/pbeast
 mailinglogger   | 5.1.0   |  TDAQ/SFO
 netifaces       | 0.11.0  |  TDAQ
 Twisted         | 21.7.0  |  TDAQ/web services
 paramiko[gssapi]| 2.7.2   |  was: TDAQ/PartitionMaker
 urwid           | 2.1.2   |  TDAQ

### Use of Python 2 based system commands

With the move to Python 3 inside the TDAQ software there is now a fundamental incompability between the normal system setup and the TDAQ environment.
If one wants to call a system command that is implemented in Python (2 for CentOS 7), like `yum` or `auth-get-sso-cookie` the environment has manipulated.

In most cases something like the following will be enough:

```shell
env -u PYTHONHOME -u PYTHONPATH auth-get-sso-cookie ...
```
If the command loads compiled libraries as well, it may be necessary to add a `-u LD_LIBRARY_PATH ` as well.

