
# Introduction 

[![Doxygen](https://img.shields.io/badge/Doxygen-9.3.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/doxygen/tdaq-09-03-00/html/index.html)
[![Javadoc](https://img.shields.io/badge/Javadoc-9.3.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-09-03-00/index.html)


The ATLAS TDAQ software version **`tdaq-09-03-00`** has been released 
on 4th May 2021.

## Availability and Installation

Outside of Point 1 the software should be used via CVMFS. It's official
location is 

    /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-03-00/

At Point 1 the software is as usual available at

    /sw/atlas/tdaq/tdaq-09-03-00/

The software
can also be installed locally via [ayum](https://gitlab.cern.ch/atlas-sit/ayum).

    git clone https://gitlab.cern.ch/atlas-sit/ayum.git
    source ayum/setup.sh

Modify the `prefix` entries in the yum repository files in `ayum/etc/yum.repos.d/*.repo`
to point to the desired destination.

    ayum install tdaq-09-03-00_x86_64-centos7-gcc8-opt

In case the LCG RPMs are not found, add this to etc/yum.repos.d/lcg.repo:

```
[lcg-repo-100]
name=LCG 100 Repository
baseurl=http://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/LCG_100/
enabled=1
prefix=[...your prefix...]
```

## Configurations

The release is available for the following configurations:

* x86_64-centos7-gcc8-opt (default at Point 1)
* x86_64-centos7-gcc8-dbg
* x86_64-centos7-gcc10-opt
* x86_64-centos7-gcc10-dbg
* x86_64-centos8-gcc10-opt (experimental)
* x86_64-centos8-gcc10-dbg (experimental)

## External Software

### LCG_100

The version of the external LCG software is [LCG_100](http://lcginfo.cern.ch/release/100/).

### TDAQ Specific External Software

 Package         | Version
 ----------------|---------
 cmzq            | 3.0.2
 zyre            | 1.1.0
 libfabric       | 1.11.0
 pugixml         | 1.9
 ipbus-software  | 2.8.0
 microhttpd      | 0.9.73
 mailinglogger   | 5.1.0
 netifaces       | 0.10.9
 Twisted         | 21.2.0
 paramiko[gssapi]| 2.7.2


### Removed packages

The `Dolozar` and the `robin_ppc` packages have been removed from the release. They are still available as packages that can be compiled locally for ATLAS users. 

### Use of Python 2 based system commands

With the move to Python 3 inside the TDAQ software there is now a fundamental incompability between the normal system setup and the TDAQ environment.
If one wants to call a system command that is implemented in Python (2 for CentOS 7), like `yum` or `auth-get-sso-cookie` the environment has manipulated.

In most cases something like the following will be enough:

```shell
env -u PYTHONHOME -u PYTHONPATH auth-get-sso-cookie ...
```
If the command loads compiled libraries as well, it may be necessary to add a `-u LD_LIBRARY_PATH ` as well.

