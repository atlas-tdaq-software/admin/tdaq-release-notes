# tdaq-09-03-00

# Introduction 

[![Doxygen](https://img.shields.io/badge/Doxygen-9.3.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/doxygen/tdaq-09-03-00/html/index.html)
[![Javadoc](https://img.shields.io/badge/Javadoc-9.3.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-09-03-00/index.html)


The ATLAS TDAQ software version **`tdaq-09-03-00`** has been released 
on 4th May 2021.

## Availability and Installation

Outside of Point 1 the software should be used via CVMFS. It's official
location is 

    /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-09-03-00/

At Point 1 the software is as usual available at

    /sw/atlas/tdaq/tdaq-09-03-00/

The software
can also be installed locally via [ayum](https://gitlab.cern.ch/atlas-sit/ayum).

    git clone https://gitlab.cern.ch/atlas-sit/ayum.git
    source ayum/setup.sh

Modify the `prefix` entries in the yum repository files in `ayum/etc/yum.repos.d/*.repo`
to point to the desired destination.

    ayum install tdaq-09-03-00_x86_64-centos7-gcc8-opt

In case the LCG RPMs are not found, add this to etc/yum.repos.d/lcg.repo:

```
[lcg-repo-100]
name=LCG 100 Repository
baseurl=http://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/LCG_100/
enabled=1
prefix=[...your prefix...]
```

## Configurations

The release is available for the following configurations:

* x86_64-centos7-gcc8-opt (default at Point 1)
* x86_64-centos7-gcc8-dbg
* x86_64-centos7-gcc10-opt
* x86_64-centos7-gcc10-dbg
* x86_64-centos8-gcc10-opt (experimental)
* x86_64-centos8-gcc10-dbg (experimental)

## External Software

### LCG_100

The version of the external LCG software is [LCG_100](http://lcginfo.cern.ch/release/100/).

### TDAQ Specific External Software

 Package         | Version
 ----------------|---------
 cmzq            | 3.0.2
 zyre            | 1.1.0
 libfabric       | 1.11.0
 pugixml         | 1.9
 ipbus-software  | 2.8.0
 microhttpd      | 0.9.73
 mailinglogger   | 5.1.0
 netifaces       | 0.10.9
 Twisted         | 21.2.0
 paramiko[gssapi]| 2.7.2


### Removed packages

The `Dolozar` and the `robin_ppc` packages have been removed from the release. They are still available as packages that can be compiled locally for ATLAS users. 

### Use of Python 2 based system commands

With the move to Python 3 inside the TDAQ software there is now a fundamental incompability between the normal system setup and the TDAQ environment.
If one wants to call a system command that is implemented in Python (2 for CentOS 7), like `yum` or `auth-get-sso-cookie` the environment has manipulated.

In most cases something like the following will be enough:

```shell
env -u PYTHONHOME -u PYTHONPATH auth-get-sso-cookie ...
```
If the command loads compiled libraries as well, it may be necessary to add a `-u LD_LIBRARY_PATH ` as well.


# BeamSpotUtils

- Updates to support new HLT histogram naming convention for Run3.
- Add support for new track-based method.
- Improvements and refactoring to support easier testing.


# CES

Package: [CES](https://gitlab.cern.ch/atlas-tdaq-software/CES)  
Jira: [ATDAQCCCES](https://its.cern.ch/jira/browse/ATDAQCCCES)  

Live documentation about recoveries and procedure can be found [here](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltErrorRecoverySystem).

## tdaq-09-03-00

**Changes in recoveries and/or procedures**:    

-   Fixed `maxEvents` procedure using wrong IS server name;
-   Handling root controller failures ([ATDAQCCCES-155](https://its.cern.ch/jira/browse/ATDAQCCCES-155));
-   Introducing synchronous actions when a transition is done ([ATDAQCCCES-156](https://its.cern.ch/jira/browse/ATDAQCCCES-156)).

**Internal changes**:       

-   OKS configuration parsing updated to follow changes in the `SwRod` schema;
-   Not sending an event to `ESPER` when its name cannot be determined;
-   Proper exception handling from the `config` layer;
-   Following changes in `PMG` Java lib;
-   EPL: using parameters in modules ([ATDAQCCCES-157](https://its.cern.ch/jira/browse/ATDAQCCCES-157));
-   EPL: the name of the root controller is no more hard-coded;
-   EPL: using hashed contexts for `PerRCApplication` and `SegmentedByProblemPerApplication`;
-   IS flag used to enable `ESPER` metrics moved to `RunParams`.


# Igui

Package: [Igui](https://gitlab.cern.ch/atlas-tdaq-software/Igui)  

## tdaq-09-03-00

**Changes exposed to users**:

-   Developers of Igui panels should add a dependency from `Jers/ers.jar` in order to compile the code;
-   A new script (`Igui_start_initial`) is available to start an IGUI in the initial partition ([ADTCC-259](https://its.cern.ch/jira/browse/ADTCC-259)). The script does not require any command line parameter;
-   The Run Control advanced panel now reports information about the actual host an application is running on ([ADTCC-223](https://its.cern.ch/jira/browse/ADTCC-223)).
    

**Internal changes**:   

-   Elog dialog: removed `FTK` from the list of systems affected
-   Handling new exceptions from the `config` layer;
-   Lazy loading of the `PMG` client interface;
-   Better reporting of errors occurring during a `GIT` commit ([ADTCC-262](https://its.cern.ch/jira/browse/ADTCC-262));
-   Proper message in case of merge conflicts ([ADHI-4822](https://its.cern.ch/jira/browse/ADHI-4822));
-   Improved error message in case of external changes to the `OKS` configuration.




# MonInfoGatherer

Alternative implementation for merging non-histogram data (ADHI-4842). This
should resolve most of the timing issues we have seen with DCM data in the
past. It is enabled by default but can be disabled with `ISDynAnyNG`
configuration parameter (see `ConfigParameters.md`).


# ProcessManager

Package: [ProcessManager](https://gitlab.cern.ch/atlas-tdaq-software/ProcessManager)  

The `ProcessManager` twiki can be found [here](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltProcessManager).

## tdaq-09-03-00

**Changes in public APIs**:

-   Java: added a method to the `PmgClient` class in order get all the processes running on a host and in the context of a defined partition.

**Internal changes**:   

-   Implemented the `daq tokens` mechanism;
-   Java: increased max file size for log retrieval;
-   Use full path of pmglauncher binary.


# RCUtils

Package: [RCUtils](https://gitlab.cern.ch/atlas-tdaq-software/RCUtils)  

## tdaq-09-03-00

-   The `initial` partition is now started as all the other partitions using `rc_bootstrap` ([ADTCC-259](https://its.cern.ch/jira/browse/ADTCC-259));
-   *setup_functions*: better error reporting when `OKS-GIT` is used;
-   Some java-based tools updated to proper handle new exceptions from the `config` layer;
-   Removed the deprecated usage of `tbb:atomic`.



# RunControl

Package: [RunControl](https://gitlab.cern.ch/atlas-tdaq-software/RunControl)  
Jira: [ATDAQCCRC](https://its.cern.ch/jira/browse/ATDAQCCRC)  

[This](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltRunControl) is the link to the main RunControl twiki.

## tdaq-09-03-00

**Internal changes**:   

-   Fixed controller dead-lock at exit ([ATDAQCCRC-47](https://its.cern.ch/jira/browse/ATDAQCCRC-47));
-   The `RootController` synchronizes with `CHIP` when a FSM state transition is completed;
-   Implemented the `daq tokens` mechanism;
-   Java: added unit tests for the client library;
-   Java: SWIG is now executed with the `-DSWIGWORDSIZE64` flag;
-   Java: handling new exceptions from the `config` layer:
-   IDL: `setError` now can throw exceptions.


**Public changes in APIs**:

-   Java: exposing the CORBA `ping` command in `CommandSender`.
 


# SFOng

- replaced deprecated tbb::tbb_hasher with std::hash
- replaced deprecated tbb::atomic with std::atomic
- fixed: starting a new run without going to shutdown (stop/start) could produce Late events
  at the beginning of the new run for a transition period


# TDAQExtJars

Package: [TDAQExtJars](https://gitlab.cern.ch/atlas-tdaq-software/TDAQExtJars)  

## tdaq-09-03-00

-   The following packages have been updated to a newer version:    
    `esper` (from 8.5.0 to **8.7.0**) 

-   The following new packages have been added:    
    `io.jsonwebtoken` (version **0.11.2**) 


# TriggerCommander

Package: [TriggerCommander](https://gitlab.cern.ch/atlas-tdaq-software/TriggerCommander)

## tdaq-09-03-00

Implementation of the `daq tokens` mechanism.


# coldpie

- tag `coldpie-00-03-06`
- add few missing methods to Database class wrapper


# config

### Java exceptions become checked

Now, the java config exceptions are derived from **ers.Issue**. This makes them checked. Most methods of config and generated DAL classes might throw an exception, that either must be caught in try / catch block or forwarded. The **config.ConfigException** base class can be used to simplify exceptions handling.

```
try
  {
    config.Configuration db = new config.Configuration("");
    dal.Partition p = dal.Algorithms.get_partition(db, "test");
  }
catch(final config.ConfigException ex)
  {
    ers.Logger.error(ex);
  }
```

Any jar using config or generated DAL must include **Jers/ers.jar** in CMakeLists.txt.

### Export configuration to Boost ptree

There are two new methods in the Configuration class to export configuration data and schema to the Boost.PropertyTree:

```
void export_schema(boost::property_tree::ptree& tree, ...);
void export_data(boost::property_tree::ptree& tree, ...);
```

The classes, objects and data files can be selected using regular expressions.

The schema ptree follows this structure:

```
{
    "class-name" {
        "abstract" : <value>
        "description" : <value>
        "superclasses" [
            <value>,
            ...
        ]
        "attributes" {
          "attribute-name" {
            "type" : <value>
            "range" : <value>
            "format" : <value>
            "is-not-null" : <value>
            "is-multi-value" : <value>
            "default-value" : <value>
            "description" : <value>
          },
          ...
        }
        "relationships" {
          "relationship-name" {
            "type" : <value>
            "cardinality" : <value>
            "is-aggregation" : <value>
            "description" : <value>
          },
          ...
        }
    }
}
```

The "superclasses", "attributes" and "relationships" nodes are only added, when non-empty. Similarly, the "description", "range", "format", "is-not-null", "is-multi-value", "default-value" and "is-aggregation" elements are only added, when meaningful.

The data ptree follows this structure:

```
{
    "class-name" {
        "object-id" {
            "attribute-name" : <value>
            "multivalue-attribute-name" [
              <value>,
              ...
            ]
            "relationship-name" : <value>
            "multivalue-relationship-name" [
              <value>,
              ...
            ]
        },
        ...
    },
    ...
}
```

A relationship value is stored in oks format: "object-id@class-name". The multivalue attributes and relationships are stored as arrays (can be empty).

There are two new utilities to export schema and data into json, xml and ptree-info formats using above methods:
* config_export_schema
* config_export_data

### Rename object in python binding

The python binding supports now renaming an OKS object.

```python
import config

db = config.Configuration('oksconfig:myfile.data.xml')
hltsv = db.get_obj('HLTSVApplication', 'HLTSV')
hltsv.rename('HLTSV-new-name')
db.commit('Changed name of HLT supervisor application')
```

On the DAL level:

```python
import config

db = config.Configuration('oksconfig:myfile.data.xml')
hltsv = db.get_dal('HLTSVApplication', 'HLTSV')
hltsv.rename('HLTSV-new-name')
db.update_dal(hltsv)
db.commit('Changed name of the HLT supervisor application')
```



# dbe

Package: [dbe](https://gitlab.cern.ch/atlas-tdaq-software/dbe)  
Jira: [ATLASDBE](https://its.cern.ch/jira/browse/ATLASDBE)  

## tdaq-09-03-00

-   The GIT checkout directory is properly identified so that the user can use it to add new files;
-   The object editor widget is refreshed in case of a database reload;
-   Unit tests retrieve configuration from `GIT`.


# dcm

## tdaq-09-03-00
Fix for Jira ADHI-4503, added ERS_DEBUG messages for easing debugging.
Moved all python scripts (dcm/test) to python3.


# DVS GUI (graphical UI for DVS)
See also related [DVS](https://gitlab.cern.ch/atlas-tdaq-software/dvs) and [TestManager](https://gitlab.cern.ch/atlas-tdaq-software/TM) packages.



# dynlibs - Load shared libraries

This package is deprecated from tdaq-09-00-00 onwards.

Please use plain `dlopen()` or [boost::dll](https://www.boost.org/doc/libs/1_72_0/doc/html/boost_dll.html)
instead. Note that unlike in this package, the `boost::dll::shared_library` object has to stay in 
scope as long as the shared library is used !

## Example of possible replacement

```c++
#include <boost/dll.hpp>

double example()
{
   boost::dll::shared_library lib("libmyplugin.so",
                                  boost::dll::load_mode::type::search_system_folders |
                                  boost::dll::load_mode::type::rtld_now);
   // Get pointer to function with given signature
   auto f = lib.get<double(double, double)>("my_function")
   return f(10.2, 3.141);
}
```

# emon

 * Connections between event samplers and monitors have been optimized. Existing configurations should be adjusted to benefit form that. Previously the _NumberOfSamplers_ parameter has been used to define a number of samplers to be connected by all monitors of a group that uses the same selection criteria. In the new implementation this number defines a number of samplers that each individual monitor has to connect. That makes no difference for monitors that used to connect to a single sampler and do't form a group. For the monitors that share the same selection criteria, like for example the Global Monitoring tasks, this number should be changed to the old number divided to the number of the monitors in the group. For Athena monitoring the corresponding parameter of a JO file is called _KeyCount_.
# jeformat

Package: [jeformat](https://gitlab.cern.ch/atlas-tdaq-software/jeformat)  


# ls

Package: [ls](https://gitlab.cern.ch/atlas-tdaq-software/ls)  



# mda

Small fix in mda_register script, updates for Python3.


# OKS

### Show hidden oks data xml attributes for text editors

Use new oks_refresh utility with -x option to enforce all attributes to be shown.

### Deprecated OWL date/time format

The OWL date/time formats are deprecated and will be removed in next public release. ERS warning is reported by oks library, when a file containing data stored in deprecated format is loaded.
Such file can be refreshed using oks editors or new **oks_refresh** utility. For example:
```
$ git clone <url> .
$ TDAQ_DB_PATH=`pwd`:$TDAQ_DB_PATH oks_refresh -f ./<file>
$ git commit -m "refresh <file> to update date/time format" ./<file>
$ git push origin master
```

### Ordering of multi-value attributes and relationships

Use new **ordered** attribute for multi-value attribute or relationship to sort its data on save. The default value is **no**.
Implement sort of multi-value attributes and relationships in OKS data editor.



# owl

The `owl` package consisted for historical reasons of at least three
different functional areas, and has been split after release tdaq-09-02-01.

The process management related functionality is now in the 
new [pmgsync](https://gitlab.cern.ch/atlas-tdaq-software/pmgsync) package.
If you include any of these headers check carefully if you really need
them, as most of this is handled internally by the various run control
libraries.

The [threadpool](https://gitlab.cern.ch/atlas-tdaq-software/threadpool)
package has been separated as well. As it is not used in TDAQ
itself, it is no longer part of the release. The git repository contains
a functioning package that can be dropped into a release if needed.
There is also a thread pool implementation in the [ipc](https://gitlab.cern.ch/atlas-tdaq-software/ipc/-/blob/master/ipc/threadpool.h)
package.

The following classes in the `owl` package have been deprecated and
will be removed in a future release:

  * OWLThread (use `std::thread` instead)
  * OWLMutex and related mutex and lock classes (use `std::mutex` etc. instead)
  * OWLCondition (use `std::condition_var` instead)

There is currently no replacement for `OWLSemaphore` in the C++17 
standard, however, there will be [one](https://en.cppreference.com/w/cpp/thread/counting_semaphore) 
in the upcoming C++20. If you only use the class to stop your main thread when a 
signal is received in another thread, consider using the new helper functions in
[ipc/signal.h](https://gitlab.cern.ch/atlas-tdaq-software/ipc/-/blob/master/ipc/signal.h) instead.

# P-BEAST

* add **FillGaps** parameter to various queries allowing to define filling empty intervals, when downsample data:
    * **none** - no any extra data points are inserted, if there are no source data points
    * **near** - add approximated data points before and after interval of existing source data points to avoid undesired linear approximation on long empty intervals (previous behavior)
    * **all** - add approximated data points into all empty intervals


# rn (Run Number)

# tdaq-09-01-00

Jira: [ADTCC-242](https://its.cern.ch/jira/browse/ADTCC-242) 

Store start of run timestamp and run duration with nanoseconds resolution.
Store TDAQ release name.
If oks repository is used, store oks config version into run number database and tag oks repository by run number / partition name.
# swrod

### Data Alignment

Fragment building algorithms have been modified to produce 4-byte aligned data. Both GBT and FULL mode algorithms expect that every incoming data chunk has a size that is multiple of 4 bytes. If this is not the case the algorithms add padding zeros at the end of the chunk to make it 4-byte aligned.

### Public API changes

The type of the **swrod::ROBFragment::m_data** attribute has been changed. This affects custom processing code that deals with ROB fragments payload. See the updated [User's Guide](https://gitlab.cern.ch/atlas-tdaq-software/swrod#rob-fragment-memory-management) for the detailed explanation of the new API.

### Configuration schema changes

* A new class **SwRodMonitoringApplication** has been added. It can be used in place of the normal **SwRodApplication** class if data fragments produced by the corresponding SW ROD shall not be used for event building and therefore shall be hiden from HLT.

* **SwRodModule** class has a number of new parameters:
    * **InputMethod** relationship has been moved to this class from the **SwRodRob** one. This is done to be able to share the same input object by multiple ROBs if they belong to the same module.
    * **CPU** attribute has been moved to this class from the **SwRodRob** one. It can be used to set CPU affinity for the data receiving threads. 
    * **WorkersNumber** attribute has been moved from the **SwRodFragmentBuilder** class. This attribute defines the number of data receiving threads that will be used by the input object.
    * **L1AHandler** is a new optional relationship. This relationship allows to define a specific TTC-to-Host e-link for the ROBs that are referenced by a given module. If this relationship is left empty then TTC-to-Host e-link from the default **L1AHandler** object referenced by the **SwRodConfiguration** will be used instead. If the latter is also empty then fragment building will be data-driven.

* Two attributes of the **SwRodFragmentBuilder** class have been renamed, which affects as well **SwRodFullModeBuilder** and **SwRodGBTModeBuilder** classes, which inherit from it:
    * **SendersNumber** attribute has been renamed to **BuildersNumber**
    * **FlashBufferAtStop** attribute has been renamed to **FlushBufferAtStop**

* A new attribute **FlushBufferAtStop** has been added to the **SwRodFragmentConsumer** class. This attribute is inherited by all specific Consumer classes.

* **BufferPages** and **BufferPageSize** attributes have been removed from the **SwRodNetioNextInput** as now these parameters are taken directly from the FelixBus where they are published by the service providers.

* The type of **CPU** attribute of the SW ROD configuration classes have been changed from integer to string. A string value may contain comma separated list of CPU core numbers and optionally may contain ranges, for example: 0,5,7,9-11. By default the attribute value is set to an empty string, which means that no affinity is set for the corresponding worker threads.



# transport

Clients using classes from the `transport` package should
look into more modern network libraries like 
[boost::asio](https://www.boost.org/doc/libs/1_73_0/libs/asio/)
until the C++ standard contains an official network library.

