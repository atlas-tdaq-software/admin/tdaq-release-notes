# ATLAS Trigger/DAQ Software

Here you can find release notes for recent versions of the ATLAS TDAQ software.

## Release Notes

* [Nightly Build](release-notes/nightly)
* [tdaq-12-00-00](release-notes/tdaq-12-00-00)
* [tdaq-11-02-00](release-notes/tdaq-11-02-00)
* [tdaq-10-00-00](release-notes/tdaq-10-00-00)
* [tdaq-09-04-00](release-notes/tdaq-09-04-00)
* [tdaq-09-03-00](release-notes/tdaq-09-03-00)
* [tdaq-09-02-01](release-notes/tdaq-09-02-01)
* [tdaq-09-01-00](release-notes/tdaq-09-01-00)
* [tdaq-09-00-00](release-notes/tdaq-09-00-00)

## Other Information

* [CMake Information for Developers](https://twiki.cern.ch/twiki/bin/viewauth/Atlas/DaqHltCMake)
* [Release Notes for older versions](https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw)
* Mailing list: [atlas-tdaq-software@cern.ch](mail:atlas-tdaq-software@cern.ch)
    * Subscribe via [e-group](https://e-groups.cern.ch/e-groups/EgroupsAddNewMember.do)
* Found a bug or have a feature request ? [Jira](https://its.cern.ch/jira/browse/ADHI)

