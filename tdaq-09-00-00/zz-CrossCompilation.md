# Cross Compilation

This release has all changes in the necessary CMake files to support
some basic cross-compilation. This has only been tested with the
`aarch64` architecture (ARM) and using the same CentOS 7 version
on the target as on the host.

Only a small subset of the LCG  software is available for ARM.
A reasonable subset of `tdaq-common` and `tdaq` can be cross-compiled 
so that a run-controlled application can be developed for the ARM target
sytem.

For more details look at [the basic cross-compilation setup](https://gitlab.cern.ch/atlas-tdaq-software/arm-cross-compiler)
and [the TDAQ cross-compilation project](https://gitlab.cern.ch/atlas-tdaq-software/tdaq-arm).