# Removed Packages

The following packages have been removed since `tdaq-08-03-01`:

* [FarmTools](https://gitlab.cern.ch/atlas-tdaq-software/FarmTools)
* [OnlinePolicy](https://gitlab.cern.ch/atlas-tdaq-software/OnlinePolicy) (the `getcmtconfig` script moved to 
  [DAQRelease](https://gitlab.cern.ch/atlas-tdaq-software/DAQRelease)
* ROx - all packages related the SW ROD prototype

 