
# tdaq-11-02-00

[![Doxygen](https://img.shields.io/badge/Doxygen-11.2.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/doxygen/tdaq-11-02-00/html/index.html)
[![Javadoc](https://img.shields.io/badge/Javadoc-11.2.0-informational)](https://test-tdaq-sw.web.cern.ch/test-tdaq-sw/code/javadoc/tdaq-11-02-00/index.html)


The ATLAS TDAQ software version **`tdaq-11-02-00`** has been released
on December 1st, 2023.

Update: **`tdaq-11-02-01`** has been built on January 31st, 2024. The only
difference is a new ROOT version which fixes a critical bug.

## Availability and Installation

Outside of Point 1 the software should be used via CVMFS. It's official
location is

    /cvmfs/atlas.cern.ch/repo/sw/tdaq/tdaq/tdaq-11-02-00/

At Point 1 the software is as usual available at

    /sw/atlas/tdaq/tdaq-11-02-00/

The software can also be installed locally via [ayum](https://gitlab.cern.ch/atlas-sit/ayum).

    git clone https://gitlab.cern.ch/atlas-sit/ayum.git
    source ayum/setup.sh

Modify the `prefix` entries in the yum repository files in `ayum/etc/yum.repos.d/*.repo`
to point to the desired destination.

    ayum install tdaq-11-02-00_x86_64-el9-gcc13-opt

Note that `ayum` is not supported beyond CentOS 7. Instead switch to the
new [atlas-dnf5](https://gitlab.cern.ch/atlas-tdaq-software/atlas-dnf5) tool. Pick
the latest [release](https://gitlab.cern.ch/atlas-tdaq-software/atlas-dnf5/-/releases).

In case the LCG RPMs are not found with ayum, add this to etc/yum.repos.d/lcg.repo:

```
[lcg-104-centos7-x86_64]
name=LCG 104 Releases (CentOS 7)
baseurl=https://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/x86_64/LCG_104/
prefix=...your-prefix...
gpgcheck=0
enabled=0
protect=0

[lcg-104-el9-x86_64]
name=LCG 104 Releases (EL 9)
baseurl=https://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/9/x86_64/LCG_104/
prefix=...your-prefix...
gpgcheck=0
enabled=0
protect=0

[lcg-packs-centos7-x86_64]
name=LCG Release Packages (CentOS 7)
baseurl=https://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/7/x86_64/Packages/
prefix=...your-prefix...
gpgcheck=0
enabled=0
protect=0

[lcg-packs-centos9-x86_64]
name=LCG Release Packages (CentOS 9)
baseurl=https://lcgpackages.web.cern.ch/lcgpackages/lcg/repo/9/x86_64/Packages/
prefix=...your-prefix...
gpgcheck=0
enabled=0
protect=0
```

## Configurations

The release is available for the following configurations:

  * x86_64-el9-gcc13-opt (default at Point 1)
  * x86-64-el9-gcc13-dbg (debug version at Point 1)
  * x86_64-centos7-gcc11-opt (legacy)
  * x86_64-centos7-gcc11-dbg (legacy)
  * aarch64-el9-gcc13-opt (experimental)

The EL 9 variant recognizes all of CentOS Stream, Rocky Linux,
Alma Linux, RedHat Enterprise Linux as equivalent. Note that
the tag for the operating system has changed from centos9 to
el9. This may also require changes in OKS databases that are
copied over from older releases

## External Software

### LCG_104c

The version of the external LCG software is [LCG_104c](http://lcginfo.cern.ch/release/104c/).

Update: The LCG version for tdaq-11-02-01 is [LCG_104d](http://lcginfo.cern.ch/release/104d/) where
ROOT was updated to v6.28.12.

### TDAQ Specific External Software

 Package         | Version |  Requested by
 ----------------|---------|----------------
 cmzq            | 4.2.1   |  FELIX
 zyre            | 2.0.1   |  FELIX
 colorama        | 0.4.4   |  DCS
 opcua           | 0.98.13 |  DCS
 parallel-ssh    | 2.10.0  |  TDAQ (PartitionMaker)
 pugixml         | 1.9     |  L1Calo, L1CTP
 ipbus-software  | 2.8.12  |  L1Calo, L1CTP
 microhttpd      | 0.9.73  |  TDAQ (pbeast)
 mailinglogger   | 5.1.0   |  TDAQ (SFO)
 Twisted         | 22.4.0  |  TDAQ (webis_server)
 urwid           | 2.1.2   |  TDAQ
 jwt-cpp         | v0.6.0  |  TDAQ
 Flask           | 2.1.3   |  TDAQ
 flask-sock      | 0.6.0   |  TDAQ (Phase II)
 gunicorn        | 21.2.0  |  TDAQ (Phase II)
 python-ldap     | 3.4.3   |  TDAQ (Phase II)

Note that `libfabric` has been removed from the TDAQ externals.

## New and Removed Packages

The following packages have been added since tdaq-10-00-00:

  * FELIXPyTools
  * ers2idl
  * sso-helper

The following packages have been removed since tdaq-10-00-00:

  * ROSTCPNP
  * felixbus
  * netio
  * pmaker
  * trp_gui
  * TriggerTool
  * TrigDb
  * BunchGroupUpdate

## OCI and Apptainer Images

You can mostly replace `podman` with `docker` in the following examples.

The OCI images used for building and testing the TDAQ software are
available here:

```shell
podman pull gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-el9
podman pull gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:aarch64-el9
podman pull gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos7
podman pull gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:aarch64-centos7
```

A multi-arch image for both Intel and ARM64 is available as

    gitlab-registry.cern.c/atlas-tdaq-software/tdaq_ci:el9

Run it like this:

```shell
podman run -it --rm -v /cvmfs:/cvmfs:ro,shared gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-el9
```

The corresponding apptainer/singularity images are available on CVMFS:

```
/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-el9
/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:aarch64-el9
/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos7
/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:aarch64-centos7
```

Run it like this:

```shell
apptainer shell -p -B /cvmfs /cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-centos7
```
The `-p` option puts you in a separate PID namespace, so you only see the processes inside the container when
you use `ps xuwf`. It will also kill all of your processes when you exit the container.

In case you have no local CVMFS, build your own SIF file:

```shell
apptainer build tdaq-el9.sif docker://gitlab-registry.cern.ch/atlas-tdaq-software/tdaq_ci:x86_64-el9
apptainer shell -p -B /cvmfs tdaq-el9.sif
```

An inclusive multi-arch container that does not need CVMFS and includes the LCG software is
available here (only for for `*-el9-gcc13-opt` configuration):

```shell
podman pull registry.cern.ch/atlas-tdaq/tdaq:11.2.0
podman run --it --rm registry.cern.ch/atlas-tdaq/tdaq:11.2.0
```

### Graphical UIs in a container

GUIs mostly don't work out of the box in a container. `apptainer` should work out of the
box with the instructions above.

For `podman` use the following additional arguments:

```shell
podman run -it -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY -h $HOSTNAME --rm registry.cern.ch/atlas-tdaq/tdaq:11.2.0
```

`docker` is the most complicated. If you plan to use the container regularly, the best way is to
create a customized version just for you: (replace the `uid` and `user` arguments with your local
user ID and user name.

```Dockerfile
FROM registry.cern.ch/atlas-tdaq/tdaq:11.2.0
ARG uid=1000
ARG user=rhauser
RUN useradd -U -u ${uid} -m ${user}
USER ${user}
```

Then run:

```shell
docker build -t my/tdaq:11.2.0 .
```

Alternatively you can overwrite the arguments from the command line by adding `--build-arg uid=2000 --build-arg user=myself`.

Finally run it like this:

```shell
docker run -it -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$DISPLAY -h $HOSTNAME --rm my/tdaq:11.2.0
```

### Inside the container

The above examples put you into a container where you have the same user ID as outside. This also simplifies
the use case where you want to mount e.g. a directory with files from the host into the container.

```shell
. /etc/profile.d/tdaq.sh
cm_setup tdaq-11-02-00
export TDAQ_IPC_INIT_REF=file:/tmp/init.ref
cd /tmp
pm_part_hlt.py -p test
setup_daq test.data.xml initial
setup_daq test.data.xml test
```

You should get the IGui on your screen and be able to interact with it.

## Setup Options

In the following we assume some alias like

```shell
alias cm_setup='source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh'
```

  * The `cm_setup --list` option will show the available releases including nightlies
  * The `cm_setup --clean ...` option will bypass all testbed specific setup. This is
    useful if you want to use testbed hardware but be completely independent from the
    existing infrastructure. You have to set your own `TDAQ_IPC_INIT_REF` path to
    start a private initial partition, if needed.
  * The `cm_setup` script takes a short version of the CMTCONFIG build configuration
    as argument. E.g.
    * `cm_setup nightly dbg` will setup `x86_64-el9-gcc13-dbg`
    * `cm_setup nightly gcc13` will setup `x86_64-el9-gcc13-opt`
    * `cm_setup nightly gcc12-dbg` will setup `x86_64-el9-gcc13-dbg`
    * There is no short cut for setting the architecture or the OS
