#!/bin/bash
#
# This needs write access to the repository so
# it should run as e.g. an acrontab job.
# 
# Arguments are the target release, and the version to fence off.
#
# update-release-notes.sh tdaq-09-04-00 tdaq-09-03-00
#
[ $# -ne 2 ] && exit 1
d=$(mktemp -d) || exit 2 
trap 'rm -rf $d' EXIT

cd $d
git clone -q https://:@gitlab.cern.ch:8443/atlas-tdaq-software/admin/tdaq-release-notes.git 
cd tdaq-release-notes
./bin/collect-relnotes --to=$1 $2
git add docs 
git diff --exit-code --quiet --cached && exit 
git commit -q -am "Updated release notes for $1"
git push -q origin master
